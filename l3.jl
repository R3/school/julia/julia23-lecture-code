# shortened version of the median function
function shorter_median(vect::Vector{Float64})
    emin, emax = extrema(vect)
    half_len = length(vect) / 2
    temp = 0.0
    for i = 1:64
        temp = (emax + emin) / 2
        # quickly sum up the vectors over the line
        c = count(vect .< temp)
        # alternatively, the form below should not allocate
        # (fixed from previous version, thanks Yumeng for spotting):
        #c = count(lessthan(temp), vect)
        # the predicative form in the line above below reads roughly:
        # "count of v's that are smaller than temp in vect"
        emin, emax = c < half_len ? (temp, emax) : (emin, temp)
    end
    return temp
end

# a helper for the above. If we want to use small helper functions like this,
# it's usually best to define them at top-level so that they may be specialized
# and made efficient by the runtime.
lessthan(x) = (y) -> y<x

# read a maze
using DelimitedFiles, UnicodePlots
m = readdlm("maze.csv", ',')

# plot it
heatmap(m)

# how much do the maze cols covariate?
using StatsBase
cov(m)
heatmap(cov(m))

# can we cluster the similar rows together?
using Clustering
hclust(cov(m)) # this actually clusters by similarity, we need distance
c = hclust(exp.(.-cov(m))) # this is an ugly but effective method for converting similarities to distances

heatmap(m[:, c.order]) # similar maze columns together
heatmap(cov(m)[c.order, c.order]) # dendrogram-ordered heatmap (we can see 2 clusters! :D )

# reference maze "wave" implementation (for homework 1)
m = readdlm("maze.csv", ',', Bool)

"""
Compute a matrix with distances in a maze `m` from the specified starting point
`start`.
"""
function maze_reachability(m::Matrix{Bool}, start::Tuple{Int,Int})
    reach = zeros(Int, size(m))
    reach[start...] = 1
    step = 1
    changed = true
    while changed
        changed = false
        for y = 1:size(m, 2), x = 1:size(m, 1)
            if reach[x, y] != step
                continue
            end
            for offset in [(1, 0), (-1, 0), (0, 1), (0, -1)]
                (tx, ty) = (x, y) .+ offset
                if tx < 1 || tx > size(m, 1) || ty < 1 || ty > size(m, 2) # the tile is out of map
                    continue
                end
                if m[tx, ty] # there's a wall
                    continue
                end
                if reach[tx, ty] != 0 # we were there already
                    continue
                end
                reach[tx, ty] = step + 1
                changed = true
            end
        end
        step += 1
    end
    return reach
end

# test it
heatmap(maze_reachability(m, (1, 1)))

# test on the big maze
m = readdlm("maze-big.csv", ',', Bool)
heatmap(maze_reachability(m, (1, 2)), width = size(m, 2), height = size(m, 1))
# benchmark on "maze-bigger.csv"!

# graph layouting implementation (for homework 2)
edges = readdlm("graph.csv", ',', Int)
nodes = rand(maximum(edges), 2)

"""
Spring-like layouting function that updates `nodes` in place for `iters`
iterations scaled for time step Δt.
"""
function layout!(iters::Int, Δt::Float64, edges::Matrix{Int}, nodes::Matrix{Float64})
    for i = 1:iters
        forces = zeros(size(nodes))
        # attractive forces of springs (edges); I simplified the springs to zero-length ones :]
        for j = 1:size(edges, 1)
            dist = sqrt(sum((nodes[edges[j, 2], :] .- nodes[edges[j, 1], :]) .^ 2))
            force = 0.5 * dist^2
            vect_dist = nodes[edges[j, 2], :] .- nodes[edges[j, 1], :]
            scaled_dist = force .* vect_dist
            for d = 1:2
                forces[edges[j, 1], d] += scaled_dist[d]
                forces[edges[j, 2], d] -= scaled_dist[d]
            end
        end
        # repulsive forces between all nodes that are close
        for j = 1:size(nodes, 1)
            for k = 1:size(nodes, 1)
                if j == k
                    continue
                end
                vect_dist = nodes[k, :] - nodes[j, :]
                dist = sqrt(sum(vect_dist .^ 2))
                vect_dist ./= dist^2
                forces[j, :] .-= vect_dist
            end
        end
        # move the nodes according to the forces
        for j = 1:size(nodes, 1)
            for k = 1:size(nodes, 2)
                nodes[j, k] += Δt * forces[j, k]
            end
        end
    end
end

# test the spring-like layouting
layout!(100000, 0.001, edges, nodes);
scatterplot(nodes[:, 1], nodes[:, 2], color = 1:size(nodes, 1));

# test on a slightly larger graph
# (this one contains a hexagon cycle connected to a triangle cycle with 2 "leafs")
edges = readdlm("graph-big.csv", ',', Int)
nodes = rand(maximum(edges), 2)
layout!(100000, 0.001, edges, nodes);
scatterplot(nodes[:, 1], nodes[:, 2], color = 1:size(nodes, 1));

# Hints for homework:
# Better traversing of the matrices (applicable for both nodes and edges)
for (v1, v2) in eachrow(edges)
    @info "edge!" v1 v2
end

# you can ignore iteration variables that you don't mind using:
for _ in something
    # ...
end

# easier way to compute Euclidean distances
using Distances
distf = Euclidean()
distf(nodes[1, :], nodes[2, :])
pairwise(distf, eachrow(nodes), eachrow(nodes))
