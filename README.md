
# Programming with Julia (2023) code from lectures

This repository contains code for the DSSE course for "Programming with Julia"
taking place in March '23. The code is usually just taken from what we done at
lectures, with `lX.jl` being from `X`th lecture.

Use Moodle if you have any questions/trouble with the code.
