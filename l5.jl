
# Matrix multiplication, illustration of successive improvements
# (normally just use * )
#
# Attention: do not benchmark code that is working with global variables, these
# are never type-stable (because the global env may change anytime) and the
# code that uses them thus cannot be optimized well.

# let's start with a baseline so that we know how much time is spent on moving
# the things around
function benchmark_matrix(n::Int)
    m1 = rand(n,n)
    m2 = rand(n,n)
    out = zeros(n,n)
    out .= m1 .+ m2 # this shouldn't take more time than rand()s above
    out
end

using BenchmarkTools
@benchmark benchmark_matrix(1024) # here I get like 1.2ms, let's consider that the necessary overhead.

# actual matrix multiplication (v1)
function benchmark_matrix(n::Int)
    m1 = rand(n,n)
    m2 = rand(n,n)
    out = zeros(n,n)
    for i in 1:n
        for j in 1:n
            out[i,j] = sum(m1[i,:] .* m2[:,j]) #this allocates 3 intermediate vectors
        end
    end
    out
end
@benchmark benchmark_matrix(1024) # 4.3s

# remove the overhead manually
function benchmark_matrix(n::Int)
    m1 = rand(n,n)
    m2 = rand(n,n)
    out = zeros(n,n)
    for i in 1:n
        for j in 1:n
            for k in 1:n
                # this does not allocate because it only works with statically
                # sized small things that can be put into registers
                out[i,j] += m1[i,k] * m2[k,j]
            end
        end
    end
    out
end
@benchmark benchmark_matrix(1024) # 3.5s-ish, but only the required allocs

# try to remove unnecessary writes to the array
function benchmark_matrix(n::Int)
    m1 = rand(n,n)
    m2 = rand(n,n)
    out = zeros(n,n)
    for i in 1:n
        for j in 1:n
            tmp = 0.0
            for k in 1:n
                # here we do not force writes to the array, but only modify a
                # local variable (which may be stored in a register, thus is very fast)
                tmp += m1[i,k] * m2[k,j]
            end
            out[i,j] = tmp
        end
    end
    out
end
@benchmark benchmark_matrix(1024) # 2.3s-ish now

# try to reorder the matrices so that they are accessed "along the cachelines"
function benchmark_matrix(n::Int)
    # note: transpose() alone just renumbers the matrix but does not actually
    # change the ordering in the memory!
    m1 = collect(transpose(rand(n,n)))
    m2 = rand(n,n)
    out = zeros(n,n)
    for i in 1:n
        for j in 1:n
            tmp = 0.0
            for k in 1:n
                tmp += m1[k,i] * m2[k,j]
            end
            out[i,j] = tmp
        end
    end
    out
end
@benchmark benchmark_matrix(1024) # 1.5s-ish now (almost 3x speedup!)

# this is already a bit confusing for the compiler, so we must tell it that it
# doesn't need to check array bounds.
function benchmark_matrix(n::Int)
    m1 = collect(transpose(rand(n,n)))
    m2 = rand(n,n)
    out = zeros(n,n)
    for i in 1:n
        for j in 1:n
            tmp = 0.0
            for k in 1:n
                @inbounds tmp += m1[k,i] * m2[k,j]
            end
            @inbounds out[i,j] = tmp
        end
    end
    out
end
@benchmark benchmark_matrix(1024) # 1.2s-ish now (over 3x speedup)

# Let's try to separate the problem into small blocks so that we do not thrash
# the cache so much. This separates the problem into small 3D blocks and tries
# to compute them sequentially.
function benchmark_matrix(n::Int)
    block = 8
    blocks = 1:block:n
    blockat(i) = i:(i+block-1)
    @assert mod(n,block) == 0
    m1 = collect(transpose(rand(n,n)))
    m2 = rand(n,n)
    out = zeros(n,n)
    for iblock in blocks
        for jblock in blocks
            for kblock in blocks
                for i in blockat(iblock)
                    for j in blockat(jblock)
                        @inbounds tmp = out[i,j]
                        for k in blockat(kblock)
                            @inbounds tmp += m1[k,i] * m2[k,j]
                        end
                        @inbounds out[i,j] = tmp
                    end
                end
            end
        end
    end
    out
end
@benchmark benchmark_matrix(1024) # the results here depends on the block size (first line)
# my measurements:
# block | benchmark
# 4       870ms
# 8       670ms   # this seems optimal because the block nicely fits into L1 cache
# 16      730ms
# 32      845ms
# 64      930ms

# Let's switch the cycle order
function benchmark_matrix(n::Int)
    block = 8
    blocks = 1:block:n
    blockat(i) = i:(i+block-1)
    @assert mod(n,block) == 0
    m1 = collect(transpose(rand(n,n)))
    m2 = rand(n,n)
    out = zeros(n,n)
    for iblock in blocks
        for jblock in blocks
            for kblock in blocks
                for j in blockat(jblock)
                    for i in blockat(iblock)
                        @inbounds tmp = out[i,j]
                        for k in blockat(kblock)
                            @inbounds tmp += m1[k,i] * m2[k,j]
                        end
                        @inbounds out[i,j] = tmp
                    end
                end
            end
        end
    end
    out
end
@benchmark benchmark_matrix(1024) # 635ms-ish

using .Threads
# run this with julia -t 8   (or add your desired thread count; should correspond to CPU count)

# let's make this threaded
function benchmark_matrix(n::Int)
    block = 8
    blocks = 1:block:n
    blockat(i) = i:(i+block-1)
    @assert mod(n,block) == 0
    m1 = collect(transpose(rand(n,n)))
    m2 = rand(n,n)
    out = zeros(n,n)
    @threads :static for iblock in blocks
        for jblock in blocks
            for kblock in blocks
                for j in blockat(jblock)
                    for i in blockat(iblock)
                        @inbounds tmp = out[i,j]
                        for k in blockat(kblock)
                            @inbounds tmp += m1[k,i] * m2[k,j]
                        end
                        @inbounds out[i,j] = tmp
                    end
                end
            end
        end
    end
    out
end
@benchmark benchmark_matrix(1024) # 87ms on 8 cores (over 50x total speedup!)


# This is what we ended up with at the lecture. It uses SIMD but doesn't have
# the speedup from 3D blocks (we only managed to get 2D blocks working)
function benchmark_matrix(n::Int)
    m1 = collect(transpose(rand(n,n)))
    m2 = rand(n,n)
    out = zeros(n,n)
    @threads for iblock in 1:8:n
        for jblock in 1:8:n
            for j in jblock:jblock+7
                for i in iblock:iblock+7
                    # this allocates: out[i,j] = sum(m1[i,:] .* m2[:,j])
                    out[i,j] = sum(m1[k,i] * m2[k,j] for k = 1:n)
                    #for k in 1:n
                        #out[i,j] += m1[i,k] * m2[k,j]
                    #end
                end
            end
        end
    end
    out
end
@benchmark benchmark_matrix(1024) # 150ms with 8 cores

# And just for completeness, let's compare with built-in Julia multiplication
function benchmark_matrix(n::Int)
    m1 = rand(n,n)
    m2 = rand(n,n)
    out = zeros(n,n)
    out .= m1 * m2
    out
end
@benchmark benchmark_matrix(1024) # 11ms (so there's still 8x speedup to be gathered somewhere)
