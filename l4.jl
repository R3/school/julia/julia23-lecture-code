using Symbolics
@variables a
1 + a
x = 1 + a
2 * x
y = x^2
substitute(y, Dict(a => 3))
m = substitute(y, Dict(a => y))
simplify(m)
simplify(m, expand = true)
Symbolics.derivative(m, a)
Symbolics.solve_for(m, a) # this won't work because the term isn't very linear

# completely geeky way of computing the minimum-error point between 0, 1 and 5
@variables x
f = x^2 + (x - 1)^2 + (x - 5)^2
fb = Symbolics.derivative(f, x)
Symbolics.solve_for(fb, x)

# evaluating
f_julia = Symbolics.build_function(f, x)
typeof(f_julia)
typeof(eval(f_julia)(1))
f_jul_compiled = eval(f_julia)
typeof(f_jul_compiled)

# plotting
using UnicodePlots
lineplot([i for i = 1:1000], [f_jul_compiled(i) for i = 1:1000])
lineplot(1:1000, f_jul_compiled.(1:1000)) # this transformation is likely to appear in the exam :]
lineplot(-3:0.1:10, f_jul_compiled.(-3:0.1:10))

# publication output
using Latexify
latexify(f)

# how to see what Julia sees in expressions
Meta.show_sexpr(:(5 + [n^2 for n = 1:10]))

##########################

using ForwardDiff

f(x) =
    4 * x[1]^2 - 3 * x[2]^3 + 2 * x[3]^4 + 32 - 5 * x[4] +
    2 * x[1] * x[2] * x[3] +
    2 * x[2] +
    x[2] * x[3] * x[4] +
    2 * x[1] * x[3] +
    x[4] +
    3 * (x' * x)

f([1, 2, 3, 4])
ForwardDiff.gradient(f, [1, 2, 3, 4])

t = zeros(4)
t -= ForwardDiff.gradient(f, t)
t -= ForwardDiff.gradient(f, t)
t -= ForwardDiff.gradient(f, t) # this is going to diverge

t = zeros(4)
alpha = 0.0001
@time for _ = 1:1000000
    t -= ForwardDiff.gradient(f, t) * alpha
end

println(t) #this should be a local minimum

# bonuses:
using LinearAlgebra
norm(t)

##########################

using JuMP, GLPK

m = Model(GLPK.Optimizer)

@variables(m, begin
    fa
    fb
    foods
    drinks
    coal >= 0   # the macro ensures this objective gets constrained
end)
# the macro actually sets several global variables at once,
# to enable easy use of the variables later

@objective(m, Min, coal)

@constraint(m, coal == fa + fb)
@constraint(m, foods >= 100)
@constraint(m, drinks == foods * 10 / 7)
@constraint(m, drinks == 5 * fa + 1 * fb)
@constraint(m, foods == 1 * fa + 4 * fb)   # looks like a matrix multiplication, right?

optimize!(m)

value(coal)
value(fa)
value(fb)
