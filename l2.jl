function median(x)
    x = sort(x)
    middle = length(x) ÷ 2
    x[middle+1]
end

function physicistmedian(vect)
    emin = minimum(vect)
    emax = maximum(vect)
    half_len = length(vect) / 2
    temp = (emax + emin) / 2
    for i = 1:64
        count = 0
        for elem in vect
            if elem < temp
                count += 1
            end
        end
        @info "here" count
        if count > half_len
            emax = temp
        else
            emin = temp
        end
        temp = (emax + emin) / 2
        @info "temporary" temp
    end
    return temp
end


m = readdlm("maze.csv", ',', Bool)

function nice(d)
    if d == 0
        return '.'
    else
        return '*'
    end
end

kidM = map(nice, m)

print(join(join.(eachrow(kidM)) .* "\n"))
