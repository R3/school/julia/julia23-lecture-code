
abstract type CookieNetItem end

struct Transport <: CookieNetItem
    capacity::Int
    serves::CookieNetItem
end

struct DistributionPoint <: CookieNetItem
    serves::Vector{CookieNetItem}
    ratios::Vector{Int}
end

struct Muncher <: CookieNetItem
    consumption::Int
end

# translates a JSON-ish dictionary `x` into a `CookieNetItem`-style network.
function dict2cookies(x::Dict)
    haskey(x, "type") || error("invalid cookie network specification")
    t = x["type"]
    if t == "transport"
        Transport(x["capacity"], dict2cookies(x["serves"]))
    elseif t == "distribution point"
        if length(x["serves"]) != length(x["ratios"])
            error("invalid distribution point")
        end
        DistributionPoint(dict2cookies.(x["serves"]), Int.(x["ratios"]))
    elseif t == "muncher"
        Muncher(x["consumption"])
    else
        error("invalid cookie network object")
    end
end

# building the recursion is always best from the simplest base cases
longest_chain(::Muncher) = 0
longest_chain(x::Transport) = 1 + longest_chain(x.serves)
longest_chain(x::DistributionPoint) = 1 + maximum(longest_chain.(x.serves))

# Alternative way to write the subtype handlers (Haskell folks do this all
# time), paying off especially if you have plenty of subtypes to work with.
# `go` is sometimes called `rec` if you don't expect readers to assume
# recursion.
function longest_chain_terse(x)
    go(::Muncher) = 0
    go(x::Transport) = 1 + go(x.serves)
    go(x::DistributionPoint) = 1 + maximum(go.(x.serves))
    go(x)
end

# counting the required cookies
required_cookies(x::Muncher) = x.consumption
required_cookies(x::Transport) =
    let r = required_cookies(x.serves)
        # `div` with `RoundUp` is a nice utility for computing "how many blocks we need"
        r + div(r, x.capacity, RoundUp)
    end
required_cookies(x::DistributionPoint) =
    let rs = required_cookies.(x.serves), unit = maximum(div.(rs, x.ratios, RoundUp))
        unit * sum(x.ratios)
    end

# Splitting the transports. Let's make the base case here a slightly more
# extensible -- this says basically "unless we make an exception, the network
# doesn't change", and would easily handle e.g. different types of munchers.
# Unfortunately this doesn't work for things that require their child objects
# to be modified recursively; we'd need a better recursion scheme for that.
split_transports(x::CookieNetItem) = x
split_transports(x::Transport) =
    Transport(2 * x.capacity, Transport(2 * x.capacity, x.serves))
split_transports(x::DistributionPoint) =
    DistributionPoint(split_transports.(x.serves), x.ratios)

# we help ourselves here a little, by immediately returning also the capacity
# that we found
balanced_distribution_consumption(x::Muncher) = (x, x.consumption)
balanced_distribution_consumption(x::Transport) =
    let (s, c) = balanced_distribution_consumption(x.serves)
        (Transport(x.capacity, s), c + div(c, x.capacity, RoundUp))
    end
balanced_distribution_consumption(x::DistributionPoint) =
    let scs = balanced_distribution_consumption.(x.serves)
        (DistributionPoint(first.(scs), last.(scs)), sum(last.(scs)))
    end

# this is now trivial
balanced_distribution(x) = first(balanced_distribution_consumption(x))

# this is now also trivial
# (we could do everything in 1 recursion but let's not overdo)
cookies_wasted(x) = required_cookies(x) - last(balanced_distribution_consumption(x))

# for pretty printing with indentation, it's best to have the data stored as a
# collection of "lines" as strings with the intended indents (you don't juggle
# around the lots of spaces, and re-indenting is easier)
struct Indented
    indent::Int
    str::String
end

# converts the Indented to a string with explicit spaces
render(x::Indented) = repeat(' ', x.indent) * x.str

# indentation and re-indentation
indent(s::String, i::Int = 0) = Indented(i, s)
indent(x::Indented, i::Int = 0) = Indented(x.indent + i, x.str)

# `hang` is a nice abstraction for producing indented structures; for
# simplicity this version assumes that `v` is always non-empty and the first
# item in `v` has no indentation.
hang(s::String, v::Vector{Indented}) =
    [Indented(0, s * v[1].str); indent.(v[2:end], length(s))]

# actual pretty-printing of the cookie network as the Indented structures
indented(x::Muncher) = [indent("muncher $(x.consumption)")]
indented(x::Transport) = hang("transport $(x.capacity) -> ", indented(x.serves))
indented(x::DistributionPoint) =
    vcat((hang("$r -> ", indented(s)) for (r, s) in zip(x.ratios, x.serves))...)

function prettyprint(x)
    println.(render.(indented(x)))
    nothing
end

# demo!
import JSON
@info "small network"
network = dict2cookies(JSON.parsefile("cookie-network.json"))
prettyprint(balanced_distribution(network))

@info "bigger network"
network = dict2cookies(JSON.parsefile("cookie-network-big.json"))
prettyprint(balanced_distribution(network))

# Just for a final demonstration, this is how you'd make a non-recursive
# version of the functions, which may handle infinitely nested data structures.
#
# Simpler versions exist but this one is super generic to show how to manage
# "all" possibilities: If you'd need to add parameters to the calls, you can
# add them into the tuples in the `stack` as e.g. `(:open, x, some_param)`, or
# even to `:close` calls if the implementation needs to remember anything
# between the opening and closing of the recursion.
#
# The return values are passed down on the return stack (as in FORTH and other
# smart RPN calculators) and have to be explicitly picked up when closing the
# recursion node.
#
# Notably, this "recursion framework" can be abstracted out into a single
# function that takes a nice-fied "open" and "close" functions as parameters.
# That way the recursion code would be made generic and you wouldn't ever need
# to touch the stacks directly again.
function longest_chain_nonrecursive(network::CookieNetItem)
    stack = Tuple{Symbol,CookieNetItem}[(:open, network)]
    retstack = Int[]

    # opening functions
    function open(x::Muncher)
        push!(retstack, 0) # just "return" the value
    end
    function open(x::Transport)
        push!(stack, (:close, x)) # remember that we need to close this node
        push!(stack, (:open, x.serves)) # and call for opening the child node
    end
    function open(x::DistributionPoint)
        push!(stack, (:close, x)) # again remember we need to close this
        for s in x.serves
            push!(stack, (:open, s)) # and call for opening all child nodes
        end

    end

    # leaving functions
    function close(x::Muncher)
        nothing  # this shouldn't be reached, if it ever is just don't do anything
    end
    function close(x::Transport)
        len = pop!(retstack) # get whatever the recursion child returned
        push!(retstack, 1 + len) # and return it increased by 1
    end
    function close(x::DistributionPoint)
        longest = 0
        for _ in x.serves
            # Quite conveniently, the order of elements gets flipped twice in
            # the stacks (once on pushing and once on evaluation) and here we
            # are popping the results again in the "forward" order. Here it
            # doesn't matter but for more complex functions you can be sure
            # that the order of items you get from pop!s is the same as the
            # order of calls of push!es before.
            len = pop!(retstack)
            if len > longest
                longest = len
            end
        end
        # ...also possible as: longest = maximum(pop!(retstack) for _ = x.serves)
        push!(retstack, 1 + longest) # again "return" the longest child's length + 1
    end

    # finally, run the recursion engine
    while !isempty(stack)
        (cmd, obj) = pop!(stack) # get whatever's requested next
        if cmd == :open # dispatch on open/close call
            open(obj)
        else
            close(obj)
        end
    end

    # in the end, the return stack should contain only a single item with the
    # final result
    @assert length(retstack) == 1
    first(retstack)
end
