
abstract type ZooObj end
abstract type Animal <: ZooObj end

struct Group <: ZooObj
    items::Vector{ZooObj}
end 

struct IceCreamStand <: ZooObj end

struct SignPost <: ZooObj
    ad::String
    leads_to::ZooObj
end

struct Penguin <: Animal
    name::String
end

struct Elephant <: Animal 
    weight::Float64
end

struct Lion<: Animal
    ishungry::Bool
end


animalCount(a::Animal) = 1
animalCount(a::IceCreamStand) = 0
animalCount(a::SignPost) = animalCount(a.leads_to)
animalCount(a::Group) = sum(animalCount.(a.items))

zoo = Group(
    [ Lion(false),
      Elephant(7000),
      Elephant(5000),
      SignPost("penguins here",
        Group([
          IceCreamStand(),
          Penguin("pingu"),
          Penguin("alfred"),
          Penguin("tux"),
        ])
      )
    ]
)

zooWeight(::Penguin)=2.5
zooWeight(::Lion)=100
zooWeight(e::Elephant)= e.weight
zooWeight(::IceCreamStand) = 1000
zooWeight(s::SignPost) = zooWeight(s.leads_to) + 1
zooWeight(g::Group) = sum(zooWeight.(g.items))

nudge(l::Lion) = l.ishungry ? println("roawrrrrrrrrrrrrr") : println("meow")
nudge(::Elephant) = println("Toot")
nudge(p::Penguin) = println("toot I am $(p.name)")
nudge(::IceCreamStand) = println("you wanna ice cream")
nudge(s::SignPost) = begin
    println("Entering section $(s.ad)")
    nudge(s.leads_to)
    println("Leaving section $(s.ad)")
end
nudge(g::Group) = begin
    nudge.(g.items)
    nothing
end

function toZoo(x::Symbol)
    if x == :hungryLion
        return Lion(true)
    elseif x == :happyLion
        return Lion(false)
    else
        error("Unknown symbol")
    end
end

function toZoo(x::Vector)
    return Group(toZoo.(x))
end

function toZoo(x::Pair{String, T}) where T
    return SignPost(x.first,toZoo(x.second))
end

####################################### end of stuff from the lecture ###

# I added this for completeness to show how to parse tuples
function toZoo((id,val)::Tuple{Symbol,Float64})
    if id==:elephant
        return Elephant(val)
    else
        error("unknown tuple")
    end
end

# toZoo test:
nudge(toZoo([:hungryLion, (:elephant, 123.4), :hungryLion, "more lions" => [:happyLion, :happyLion]]))

# For completeness we'll add a struct for putting things into enclosures (for safety!)
struct Enclosure <:ZooObj
    enclosed::ZooObj
end

# typically, for new datatype tree objects you first implement all the methods
# you use (except the ones that are safely defaulted using the supertypes)
animalCount(x::Enclosure) = animalCount(x.enclosed)
zooWeight(x::Enclosure) = zooWeight(x.enclosed) + 100  # fences are heavy
function nudge(x::Enclosure)
    println("Entering an enclosure")
    nudge(x.enclosed)
    println("Leaving an enclosure")
end

# a new testing zoo
zoo = Group(
    [ Lion(false),
      Elephant(7000),
      Elephant(5000),
      SignPost("penguins this way",
        Group([
          IceCreamStand(),
          Penguin("pingu"),
          Penguin("alfred"),
          Penguin("tux"),
          Enclosure(Lion(true)),
          Lion(true)
        ])
      )
    ]
)

# find if the ZOO has a hungry lion which is not in an enclosure
hasFreeHungryLion(::Enclosure) = false # everything in enclosures is enclosed, so no problem and we do not even need to recurse (!)
hasFreeHungryLion(l::Lion) = l.ishungry # a Lion "contains" a hungry lion only if it is hungry
hasFreeHungryLion(::Animal) = false # other animals do not have hungry lions
hasFreeHungryLion(::ZooObj) = false # other things do not do have these either in general
hasFreeHungryLion(s::SignPost) = hasFreeHungryLion(s.leads_to) # except there might be hungry lions behind a signpost so we need to recurse
hasFreeHungryLion(g::Group) = any(hasFreeHungryLion.(g.items)) # and if there's any hungry lion in any of the group items, it's also present in the whole group

# examples
nudge(zoo) # enclosures still work
hasFreeHungryLion(zoo) # this one actually has a hungry lion
hasFreeHungryLion(Enclosure(zoo)) # but not if you put a whole zoo into an enclosure! :]
