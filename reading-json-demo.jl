
using JSON

data = JSON.parsefile("cookie-network-big.json")

println(data["type"])
println(data["serves"][1]["type"])
println(data["serves"][1]["serves"]["type"])
println(data["serves"][1]["serves"]["serves"][1]["type"])
println(data["serves"][1]["serves"]["serves"][2]["serves"]["serves"][1]["type"])
println(data["serves"][1]["serves"]["serves"][2]["serves"]["serves"][1]["consumption"])

# trick way to make Julia format the data nicely
# (this does not satisfy the BONUS assignment!)

using JuliaFormatter
io = IOBuffer()
show(io, data)
println(format_text(String(io.data)));
